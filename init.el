
(setq inhibit-startup-message t)

(scroll-bar-mode -1)     ;Disable visible scrollbar
(tool-bar-mode -1)       ;Disable the toolbar
(tooltip-mode -1)        ;Disable tooltips
(set-fringe-mode 15)     ;Give some breathing room

(menu-bar-mode -1)       ;Disable the menu bar

;;Setup the visible bell
(setq visible-bell t)

;; Set the default face
(set-face-attribute 'default nil :font "Liberation Mono" :height 120)
;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "Liberation Mono" :height 100)
;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "Liberation Mono" :height 120 :weight 'regular)

;;(load-theme 'modus-vivendi)

;;Enable mode to quickly open recent files
(recentf-mode 1) ;;Configured in custom-keys

;;Save what you enter into minibuffer prompts
(setq history-length 25)
(savehist-mode 1) ;;Navigate history using M-n and M-p commands (similar to C-n and C-p)

;;Remember and restore the last cursor location of opened files
(save-place-mode 1)

;;Make ESCquit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;;Accept utf-8
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)
(define-coding-system-alias 'UTF-8 'utf-8)
(add-hook 'python-mode-hook
	  (lambda ()
	    (setenv "LANG" "es_MX.UTF-8")))
;;In can a buffer is already encoded in other than UTF-8, then
;;If you haven't changed the file, you can try M-x revert-buffer-with-coding-system
;;You can also use M-x recode-region
;;After you got it right, you can choose which coding system to save the file with using M-x set-buffer-file-coding-system

;;Python configuration, it wasn't needed to configure ipython
(setq python-shell-interpreter "C:/Users/grego/AppData/Local/Programs/Python/Python310/python.exe")
;;       python-shell-interpreter-args
;;       "-i C:/Python27/Scripts/ipython-script.py")


;;Initialize package sources
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;;Initialize use-package on non-Linux systems
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;;Command log package to see commands
(use-package command-log-mode
  :config
  (global-command-log-mode 1))
;;Exlude certain commands from command log mode
;; (setq clm/log-command-exceptions*
;;   '(nil self-insert-command backward-char forward-char
;;         delete-char delete-backward-char backward-delete-char
;;         backward-delete-char-untabify
;;         universal-argument universal-argument-other-key
;;         universal-argument-minus universal-argument-more
;;         beginning-of-line end-of-line recenter
;;         move-end-of-line move-beginning-of-line
;;         handle-switch-frame
;;         newline previous-line next-line))


;;Display line numbers
(column-number-mode)
(global-display-line-numbers-mode t)
;;Disable for certain modes
(dolist (mode '(org-mode-hook
		term-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;;Ivy package
(use-package swiper :ensure t)
(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
	 :map ivy-minibuffer-map
	 ("TAB" . ivy-alt-done)
	 ("C-<return>" . ivy-immediate-done))
  :config
  (ivy-mode 1))

;;Doom modeline
;;After installing all-the-icons package run M-x all-the-icons-install-fonts
;;For Linux or MacOS, that's it
;;For Windows, follow this instructions https://github.com/doomemacs/doomemacs/issues/2575
(use-package all-the-icons
  :ensure t)

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-gruvbox t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

;;Install doom-modeline to modify modeline
;;Icons are not working correctly, so turn them off temporarily t / nil
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 30)
	   (doom-modeline-icon t)
	   (doom-modeline-major-mode-icon t)
	   (doom-modeline-major-mode-color-icon t)
	   (doom-modeline-buffer-state-icon t)))

;;Install Rainbow delimiters
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;;Install Which Key
(use-package which-key
  :init (which-key-mode)
  :config
  (setq which-key-idle-delay 1))

;;Install counsel to get descriptions in M-x
(use-package counsel :ensure t
  :init
  :bind (("M-x" . counsel-M-x)
	 ("C-x b" . counsel-ibuffer)
	 ("C-x C-f" . counsel-find-file)
	 ("C-M-j" . 'counsel-switch-buffer)
	 :map minibuffer-local-map
	 ("C-r" . 'counsel-minibuffer-history))
  :config
  (counsel-mode 1)
  (setq ivy-initial-inputs-alist nil))

;;Install ivy-rich to get more details of commands
(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

;;Install helpful package
(use-package helpful :ensure t)

;;Install package to highlight indentation
(use-package highlight-indent-guides
  :ensure t
  :config
  (setq highlight-indent-guides-method 'character))
(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)


;;Install elpy the emacs python ide
(use-package elpy
  :ensure t
  :init
  (elpy-enable))
;;Install py-autopep8 to format code style
;;(use-package py-autopep8)
;;(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save) ;;Deprecated
;;(add-hook 'elpy-mode-hook 'py-autopep8-mode)
;; (add-hook 'python-mode-hook 'py-autopep8-mode)
;;Install blacken to format code style
(use-package blacken)
(add-hook 'python-mode-hook 'blacken-mode)

;;My keybindings
(define-key elpy-mode-map (kbd "<tab>") 'python-indent-shift-right)
(define-key elpy-mode-map (kbd "S-<tab>") 'python-indent-shift-left)
(define-key elpy-mode-map (kbd "C-/") 'comment-line)
(define-key elpy-mode-map (kbd "C-}") 'comment-line)
(global-set-key (kbd "M-n") (lambda () (interactive) (scroll-up-command 5)))
(global-set-key (kbd "M-p") (lambda () (interactive) (scroll-down-command 5)))

;;Install general for creatining definers / custom keys
(use-package general
  :config
  (general-create-definer kempii/custom-keys
    ;; :keymaps '(normal insert visual emacs)
    ;; :prefix "C-."
    :global-prefix "C-,"))

;;Install hydra to define custom functions
(use-package hydra)

;;Define custom hydra function for zooming in and out
(defhydra hydra-text-scale (:timeout 10)
  "scale text"
  ("+" text-scale-increase "in")
  ("-" text-scale-decrease "out")
  ("q" nil "finished" :exit t))

;;Create custom keys under kempii/custom-keys for screen configuration
(kempii/custom-keys
  "C-, s"  '(:ignore s :which-key "screen customization")
  "C-, st" '(counsel-load-theme :which-key "choose theme")
  ;;Add hydra zoom function to my custom keys
  "C-, sz" '(hydra-text-scale/body :which-key "scale text"))

;;Create custom keys under kempii/custom-keys for file customization
(kempii/custom-keys
  "C-, f"  '(:ignore f :which-key "files customization")
  ;;Open recent file using recentf-mode
  "C-, fr" '(recentf-open-files :which-key "open recent files"))

;;Create custom keys under kempii/custom-keys for utilities shortcuts
(kempii/custom-keys
  "C-, u"  '(:ignore u :which-key "utilities")
  "C-, uf" '(browse-url :which-key "firefox webpage")
  "C-, us" '(browse-url-firefox :which-key "firefox search")
  "C-, ug" '("C-x g" :which-key "magit status")
  "C-, ur" '(projectile-run-project :which-key "run project"))

;;Firefox configuration
(setq browse-url-firefox-arguments '("-search"))

;;Install projectile a project manager
(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "/home/kempii/Projects")
    (setq projectile-project-search-path '("/home/kempii/Projects")))
  (setq projectile-switch-project-action #'projectile-dired))

;;Install counsel-projectile to manage projectile using counsel
(use-package counsel-projectile
  :config (counsel-projectile-mode))

;;Install magit to manage git
(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;;Install restclient to communicate with API
;; Use restclient-mode in a text file to communicate with APIs
(use-package restclient
  :ensure t
  :mode (("\\.http\\'" . restclient-mode)))

;;Install csv-mode
(use-package csv-mode)

;;Install org
(defun kempii/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1)
  (org-display-inline-images t t))

(defun kempii/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Liberation Mono" :weight 'regular :height (cdr face)))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))

(use-package org
  :hook (org-mode . kempii/org-mode-setup)
  :config
  (setq org-ellipsis " ▾"
	org-hide-emphasis-markers t)
  (kempii/org-font-setup))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun kempii/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . kempii/org-mode-visual-fill))

;;Move customization variables to a separate file and load it
(setq custom-file (locate-user-emacs-file "custom-vars.el"))
(load custom-file 'noerror 'nomessage)
